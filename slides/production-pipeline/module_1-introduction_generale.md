
# Pipeline pour l'image de synthèse

---
## Avant de commencer


| <img src="attachements/module_1-memoire_flavio.png" width=380> | <img src="attachements/module_1-production_pipeline_fundamentals.png" width=380> |
|:-------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------:|
|                                  Flavio Perez                                   |                                      Renee Dunlop                                      |
 

---
<!-- .slide: data-stack-name="Définition & Enjeux" -->

## Qu’est-ce qu’un pipeline ?

--

### Définitions

--

> “Un pipeline est un moyen formalisé de transformer des données d’un état à un autre à travers une série de processus de gestion.”

[_Anne-Laure George-Molland_](https://www.linkedin.com/in/anne-laure-george-molland-5a21062/)

--

> “Le pipeline est la colle qui unifie le travail de tous les artistes impliqués dans une production”.

[_Production Pipeline fundamentals_](https://www.amazon.com/Production-Pipeline-Fundamentals-Film-Games/dp/0415812291)

--

> “Un ensemble de logiciels et technologies qui permettent aux artistes de travailler sur une grande quantité de tâches de façon consistante, efficiente et répétée.”

_[Mike Stein](https://www.linkedin.com/in/michaellstein)_

--

> “Un pipeline est un moyen formalisé de transformer des données d’un état à un autre à travers une série de processus de gestion.”

[_John Carey_](https://groups.google.com/g/global-vfx-pipelines)

--

> “Un pipeline est la fondation invisible aux enjeux artistiques, techniques et aux questions de productions. C’est un ensemble de procédures très organiques, qui n’a d’existence que par les artistes et techniciens qui le nourrissent en données que d’autres graphistes transforment.”

[_Flavio Perez_](http://les-fees-speciales.coop/equipe/flavio-perez/)

---

### Pourquoi utiliser un pipeline ?

--

![](attachements/module_1-pourquoi-un-pipeline.jpg)

notes:
	Sans pipeline on se retrouve rapidement avec ce genre de chose.

--

![](attachements/module_1-trouver-un-terrain.gif)

notes:
	Pour trouver un terrain d'entente afin d'avancer ensemble

--

![](attachements/module_1-synchroniser-les-artistes.gif)

notes:
Pour synchroniser les artistes/prod/sups entre eux. Finalement c’est un peu comme une musique qui va permettre de synchroniser les danseurs entre eux,

--

![](attachements/module_1-comment-ça-se-passe.gif)

notes:
C’est bien beau de vouloir tous collaborer mais finalement comment ça se passe ? Ouvrons le capot pour voir ce qui s’y cache…


---

## Une question de flux

![](attachements/module_1-flow.gif)

--

### Dataflow & Workflow

notes:
Un pipeline est une structure organique qui va gérer des flux: 
- Le flux de donnée 
- Le flux de travail 

Si on imagine la construction d’un maison par exemple…-->

--

### Workflow

> “Mouvement de travail d’un département à un autre. Le workflow décrit les differentes étapes de progression d’un projet du point de vu des départements de travail.”

[_lepipeline.org_](http://lepipeline.org/workflow.html)

notes:
Le workflow illustre l’organisation des **tâches** d’assemblage et de maitrise d’oeuvre effectuées par les ouvriers

--

### Dataflow

> “Le dataflow est l’organisation des données dans une structure de fichiers. Contrairement au workflow qui décrit des étapes de travail, le dataflow décrit les dépendances de fichiers induites par le pipeline.”

[_lepipeline.org_](http://lepipeline.org/dataflow.html)

notes:
Le dataflow consiste à gérer les matériaux de construction

---
<!-- .slide: data-stack-name="Structure" -->

## Structure

--

## Nomenclature

notes:
	Si il y a bien une chose a faire en premier lorsqu’on parle pipeline, c’est la nomenclature. C’est une fondation vitale au bon fonctionnement de la collaboration sur le projet.

--

### Définition

> La nomenclature (le naming) est l’ensemble des règles établies pour nommer les dossiers, les fichiers, voire aussi le contenu et l’organisation interne de ces fichiers

[lepipeline.org](http://lepipeline.org/nomenclature-naming.html)


--

### Objectif et enjeux de la nomenclature:

-   Communiquer <!-- .element: class="fragment" -->
-   Organiser <!-- .element: class="fragment" -->

notes:
1. Communiquer == permettre aux départements d’échanger des données et d’avoir la même compréhension de ces dernières.
2. Organiser == Structurer les fichiers <br>
De la nomenclature va directement découler la structure des fichiers.

--

![](attachements/module_1-nomenclature_flavio.png)

Arborescence développée par Flavio Perez dans son mémoire.


notes:
Mettre en place une hiérarchie de dossier intuitive :

-   La vraie utilité des dossiers est de filtrer l’information à une taille gérable.
-   C’est une technologie prévue pour cacher et non localiser les données. Les dossiers contrôles ce que vous ne pouvez pas voir et non ce que vous pouvez voir.[@dunlopProductionPipelineFundamentals2014, p107]

Finalement l’objectif d’une nomenclature:

-   Nécessaire pour permettre aux artistes de trouver les fichiers via le classique open/save
-   Donner un identifiant unique à chaque asset
-   Utiliser des identifiant lisible par l’humain
-   On peut y ajouter des informations d'état (eg. la *step*)

Si la nomenclature est bancale, la suite du pipeline le sera aussi car les composants, la structure du pipeline découle de cette dernière. Lorsque la nomenclature est définie, on peut envisager le développement de la structure du pipeline.

---

## Type d’outils

1.  Outils de conforts et d’automatisation <!-- .element: class="fragment" -->
2.  Outils d’information <!-- .element: class="fragment" -->
3.  L’asset managment <!-- .element: class="fragment" -->

notes:
Classification de flavio

--

### Outils de confort et d’automatisation

notes:
Première brique dans un pipeline ce sont les outils à destination des graphistes pour les délester de tâches redondante On les trouve à toutes les étapes de fabrication. Ils prennent souvent l’apparence d’add-on. Ce sont les outils les plus nombreux et diversifiés.Ils permettent généralement de gagner du temps. Ils peuvent avoir différents niveaux de complexité.

--

![](attachements/module_1-cube_automation.png)

Exemples d’outils de confort dans Blender à CUBE CREATIVE.

notes:
Exemple avec les outils de CUBE. Ici le cas dans un DCC. On peut constater qu’il y en a a tous les niveaux.

--

![](attachements/module_1-exemple-tube_automate.png)

Tube Automate, un exemple d’outils d’automatisation à CUBE CREATIVE.

--

### Outils d’information

![](attachements/module_1-automatisation.webp)

notes:
Ces outils récupèrent et manipulent l’information. Ils transforment le pipeline en source d’information. On a par example le suivi du temps, le status des assets,… Ces information vont permettre d’organiser le suivis de production. Ils vont mettre à jour les outils manipulé par la production tel que le **production Tracker**.

Le **production tracker** (ou manager) est le logiciel (ou ensemble d’outils) qui permet d’harmoniser les departments artistiques et techniques avec les departments de la productions. Il permet de manager des projets, traquer son avancement, faire de l’assignation, gerer des planings ou des budgets ou encore permettre des reviews.

--

![module_1-prod_trackers](attachements/module_1-prod_trackers.svg)



Quelques exemples de productions trackers

--

![](attachements/module_1-emmawhatscene.png)

Emma Whatscene, un scene manager connecté à KITSU développé à CUBE CREATIVE.

notes:
Dans les outils d’intelligence à cube nous avons par exemple Emma WhatScene, un scene manager directement connecté à CG_WIRE via l’api web de Kitsu. 

Lorsqu’une sauvegarde est effectuée, certaines données basiques vont être mises à jour sur le production tracker.

--

![](attachements/module_1-kitsu-todos.png)

Mise à jour du status des shots dans Kistu.

--

![](attachements/module_1-kitsu-timesheet.png)

Tracking du temps dans Kitsu.

--

### Bonus

Modèles de spreadsheet gratuites pour les projets étudiants

[![](attachements/module_1-spreadsheet-kitsu.png)](https://www.cg-wire.com/spreadsheets)
--

### Asset manager
- Contrôle d’accès<!-- .element: class="fragment" -->
- Automatisation<!-- .element: class="fragment" -->
- Versioning<!-- .element: class="fragment" -->

--

#### Composants d'un asset manager

<img src="attachements/module_1-asset_manager_2.svg" width=1000>
notes:
- le **data model** permet de spécifier comment les donnée gérée par l’asset manager sont structurées. En général il fourni une API pour créer des outils autour.
- le **repository** = l’infrastructure logicielle et matériel utilisé pour stocker les données

--

#### Quelques assets manager gratuits

[<img src="attachements/module_1-kabaret.png" width=150>](https://www.kabaretstudio.com/home)	
[<img src="attachements/module_1-ayon_logo.png" width=300>](https://ynput.io/ayon/)		
[<img src="attachements/module_1-tactic.png" width=300>](https://github.com/Southpaw-TACTIC/TACTIC)


--

#### Asset manager : Animation vs Jeu vidéo

|                            | Animation | Jeu vidéo|
| -------------------------- | --------- | ------------------- |
| Volume de donnée           | Important | Faible              |
| Complexité des données     | Faible    | Élevée              |
| Politique de versioning    | Basique   | Stricte             |
| Complexité des dépendances | Faible    | Complexe            |


notes:
Les spécificités d'un asset manager pour l'animation: 
- Gestion de gros volumes de données 
- Versioning basique se reposant sur la nomenclature

Les spécificités d’un asset manager pour le jeu video:

-   Beaucoup plus de type de données à gérer: level editors, animation blend-tree authoring tools, state machine editors, game scripting, etc.
-   Un versionning strict porté par un logiciel de contrôle de version.
-   Des dépendances très complexes, le code est lié aux assets et vice versa. Par exemple: Changer la longueur d’une animation pourrait involontairement impacter l’efficacité d’un personnage au combat.

Versionning: plusieurs manière de faire: 
- basique: increment 
- version control software (Perforce, Mercurial). Les intérêts: Pour l’individu: toile de sécurité - pour l’equipe: garder les ressource du projet à jour. Ça permet de faire travailler l'équipe en continue mais ça nécessite de la discipline ! Le problème que ça amène c'est que chacun travail sur son petit univers déconnecté du flux de la production. Des decisions artistique sont basé sur des asset ayant déjà changé, et des descision trechniques peuvent etre basé sur des features qui ne sont plus là !

---
<!-- .slide: data-stack-name="Étapes de production" -->

## Étapes de production

ANIMATION & JEU VIDÉO

notes:
Pour comprendre les enjeux d’un pipeline, il faut connaitre comment se fabrique un projet en image de synthèse, que ce soit un film d’animation ou un jeu vidéo. L’idée c’est donc de faire une piqûre de rappel sur les différentes étapes de conception en appuyant sur le contraste Film/Jeu.

--

### Trois grandes étapes

1.  Pré-production <!-- .element: class="fragment" -->
2.  Production <!-- .element: class="fragment" -->
3.  Post-production <!-- .element: class="fragment" -->

notes:
Globalement, que ce soit dans le jeu ou dans l’animation, on distingue 3 grandes étapes dans la conception d’un projet : **la préproduction, la production et la post production**.

--

### Pré-production

“Esquisse”

![](attachements/module_1-battle_plan.gif)

notes:
Points commun JV-Animation:
-   Art dpt: Définition du style visuel (concept, ) Previz
-   Les outils utilisés en pre-prod justifient l’emploi de TA(game) / TD(film)
-   Identifier les difficultés
-   Un temps minimiser les risques, etre sur que les idées de l’équipe de créatif sont réalisable sans couler le studio
-   Fondation du pipeline
-   Toujours se préparer au changement

--

#### Pré-prod: Animation

--
<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_1-animation_pre_prod_2.svg)


notes:
ces étape s’effectuent de manière séquentielles, linéaires.

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-animation_pre_prod_3.svg)
notes:
pour passer d’une étape à une autre on à un processus de validation avec le superviseur puis le client

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-animation_pre_prod_4.svg)
notes:
En animation, la fin de la préproduction se situe lorsque l’animatique est finie

--

#### Pré-prod: Jeux vidéo

--

![](attachements/module_1-game_pre_prod_1.svg)

notes:
Dans le jeu vidéo en general la premiere étape est de définir le concept. Ça commence avant la pré-prod et ça continu pendant.

La phase de concept: 
- GDD - game’s genre, gameplay description, features, setting, story, target audience, hardware platforms
- Évaluation et définition des techno utilisés (engine, pipeline)

La phase de pré-production diffère énormément de celle de l’animation. On parle de développement en “Slice”/“Tranche”. Chaque aspect est développé en parallèle.

Ce qu’on qualifie de tranche horizontale représente un grand nombre de features dans un environment de prototypage surnommé “whitebox-level”. Ces derniers contiennent des assets non finis, des animation manquantes etc… Ils ont juste pour objectifs d’aider à voir comment le gameplay se déroulera, combien de temps le jeu durera Comme le gameplay guide l’art, plus il y aura de tranches horizontale, moins il sera nécessaire de re-définir le contenu artistique durant la production

--

![](attachements/module_1-whitebox.jpg)

Un exemple de “whitebox level”.

--
<!-- .slide: data-transition="none" -->
![](attachements/module_1-game_pre_prod_2.svg)

notes:
A l’inverse, ce qu’on appel les “Vertical slices” son des petites sections du jeux aussi proche que possible de la qualité du livrable Amener de la confiance dans le développement du jeu, ça permet également de stress test le pipeline Les vertical slice sont les porte pour la production

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_pre_prod_4.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_pre_prod_3.svg)

notes:
Au terme de la pré-poduction, si le publisher aime la démo, on dit que le jeu sera **green-lighté**.

--

### Pré-prod: Enjeux

![](attachements/module_1-enjeux-preprod.png)

--

![](attachements/module_1-train.gif)

> “La seule chose qui est constante : c’est le changement.”

[_Production Pipeline fundamentals_](https://www.amazon.com/Production-Pipeline-Fundamentals-Film-Games/dp/0415812291)

--

### Production

“Détail”

![600](attachements/module_1-house.gif)

notes:
Création de tout les assets. La différence jeux video/animation arrive lors de l’assemblage des assetS en de plus grand groupe
- Anmation: Shot creation, pas d’enjeux de taille 
-  Jeu: Level creation, enjeux hardware

--

#### Production: Animation

--
<!-- .slide: data-transition="none" -->

![|600](attachements/module_1-animation_prod_3.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-animation_prod_4.svg)

--
<!-- .slide: data-transition="none slide-out" -->

![](attachements/module_1-animation_prod_5b.svg)

--

<img src="attachements/module_1-TBDL.jpg" width=1000/>

La TBDL, l’outil utilisé pour faire le dépouillement des shot à CUBE.

--
<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_1-animation_prod_6.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-animation_prod_7.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-animation_prod_9.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-animation_prod_10.svg)

--

#### Production: Jeu vidéo

--

![](attachements/module_1-game_prod_2.svg)

notes:
la production du jeu vise principalement à créer le contenu Le pipeline devrait seulement avoir des fixes mais il peut être amené à changer en cours de route lorsqeu’il révèle ses faiblesses

--

<img src="attachements/module_1-star_cityzen_production_schedule.jpg" height=800>

Exemple avec le plan de production de Star Citizen.

--
<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_1-game_prod_3.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_prod_4.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_prod_5.svg)

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_prod_6.svg)

--

### Post production / Finalisation

![](attachements/module_1-last_chance.gif)

notes:
Dernière chance d’améliorer le produit. Le moment de faire un post mortem À ce moment là les développeurs pipeline peuvent préparer les projets qui arrivent

--

### Post Prod: Animation


![](attachements/module_1-animation_post_prod_2.svg)



--

### Jeu vidéo: Finalling

--
<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_1-game_finalling.svg)

notes:
Similaire à la finalisation d’un logiciel en développement (QA, Quality assurance, bulletproofing) Au début de la phase Finale, le jeu est jouable mais très buggé - bug de code - bug d’asset (collisions, etc…) Constitué d’une série de phases

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_finalling_2.svg)

notes:
**Content complete**: tout les placeholders ont été remplacé par une version shippable de l’asset Ces dernier respectent les contraintes techniques Pas de contenu additionnel sera rajouté L’histoire est vérouillée

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_finalling_3.svg)

notes:
**Beta**:Tout le contenu du gameplay est complet, le jeu est sous forme de livrable en terme de qualité et d’asset et d’utilisation des ressources. C’est une version buggé de la version final sur laquelle se déroule des playtest et des demo publique. Asset polishing and bug fixing. ! Communication is essential !

--
<!-- .slide: data-transition="none" -->

![](attachements/module_1-game_finalling_4.svg)

notes:
**Final**: content is locked, only fix accepted. only senior are able to make last minute changes. Test de QA  
Gold Version

---

## Pour aller plus loin

<small>

-  Bethke, Erik. **Game Development and Production**. Wordware Pub, 2003.
-   Bettis, Dane Edward. **Digital Production Pipelines: Examining Structures and Methods in the Computer Effects Industry**. Texas A&M University, 2005. [oaktrust.library.tamu.edu](http://oaktrust.library.tamu.edu), [https://oaktrust.library.tamu.edu/handle/1969.1/2406](https://oaktrust.library.tamu.edu/handle/1969.1/2406
-   Dunlop, R. Production Pipeline Fundamentals for Film and Games. CRC Press, 2014, [https://books.google.fr/books?id=a2PMAgAAQBAJ](https://books.google.fr/books?id=a2PMAgAAQBAJ).
-   Flavio Perez.**Le Pipeline de l’image de Synthèse : Définitions et Enjeux Pour Les oeuvres Collaboratives**.
-   “**Game Development Glossary: The Vertical Slice**” Ask a Game Dev, [https://askagamedev.tumblr.com/post/77406994278/game-development-glossary-the-vertical-slice](https://askagamedev.tumblr.com/post/77406994278/game-development-glossary-the-vertical-slice). Accessed 8 Nov. 2020.
-   **Game Production Cycle | Game Production**. [https://leonardperez.net/game-production-cycle/](https://leonardperez.net/game-production-cycle/). Accessed 8 Nov. 2020.
-   Labschutz, Matthias, and Katharina Krosl. **Content Creation for a 3D Game with Maya and Unity 3D**. p. 9.

</small>

--

<small>

-   **Lepipeline.Org**. [http://lepipeline.org/](http://lepipeline.org/). Accessed 9 Nov. 2020.
-   Murphy-Hill, Emerson, et al. **Cowboys, Ankle Sprains, and Keepers of Quality: How Is Video Game Development Different from Software Development?**. Proceedings of the 36th International Conference on Software - Engineering - ICSE 2014, ACM Press, 2014, pp. 1–11. [DOI.org](http://DOI.org) (Crossref), doi:10.1145/2568225.2568226.
-   Musil, Juergen, et al. **Improving Video Game Development: Facilitating Heterogeneous Team Collaboration through Flexible Software Processes.** Systems, Software and Services Process Improvement, edited by Andreas Riel et al., vol. 99, Springer Berlin Heidelberg, 2010, pp. 83–94. [DOI.org](http://DOI.org) (Crossref), doi:10.1007/978-3-642-15666-3_8.
-   Ramadan, Rido, and Yani Widyani. **Game Development Life Cycle Guidelines.** 2013 International Conference on Advanced Computer Science and Information Systems (ICACSIS), IEEE, 2013, pp. 95–100. [DOI.org](http://DOI.org) (Crossref), doi:10.1109/ICACSIS.2013.6761558.
-   **Technologies, Unity. Common Game Development Terms and Definitions | Game Design Vocabulary |** Unity. [https://unity.com/how-to/beginner/game-development-terms](https://unity.com/how-to/beginner/game-development-terms). Accessed 8 Nov. 2020. Toftedahl, Marcus, and Henrik Engström. A Taxonomy of Game Engines and the Tools That Drive the Industry. p. 17.

</small>

---

## Des questions ?
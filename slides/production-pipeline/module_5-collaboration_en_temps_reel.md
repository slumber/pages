
## Collaboration temps réel pour l’image de synthèse


---
<!-- .slide: data-stack-name="Constat" -->

### Constat

--

![](attachements/module_5-recall.gif)
notes:
on va revenir en arrière dans les cours pour voir les problèmes récurrents aux productions actuelle et en quoi le collaboratif temps réel peut changer les choses :)

--

![](attachements/module_5-gantt_vfxguide.jpg)
notes:
On a vus lors du cours sur les méthodologies agiles on a vu que la majorité des projets en animation étaient gérés en “cascade”. Avec les fameux gantt de la première guerre mondiale !

--

<img src='attachements/module_5-animation_workflow6.svg' width=1000>

notes:
Cela transparait très clairement dans tout la production telle qu’on la connnais aujourd’hui, tel que je vous l’ai décrite dans cette série de cours.

Le workflow abordé dans le premier cours reflète directement cette réalité, les étapes de production linéaires et interdépendantes sont la conséquence directe de ce mode de planification pour le moins primitif :D

--

<img src='attachements/module_5-animation_workflow_nodes_22.svg' width=1000>

notes:
Pour supporter ce workflow, on avait vu que le dataflow ressemblait à ça,…(2-3 ieme cours) tout aussi linéaire ce qui est normal car pour rappel le dataflow va être là pour supporter le workflow !

--

![](attachements/module_5-cars.gif)

notes:
Enfait on peut constater (sans jugement) qu’à l’heure actuelle on produit des films comme des voitures.

--

![](attachements/module_5-money3.gif)
notes:
On a adopté ce modèle de production pour sa rentabilité et sa prévisibilité (le fameux gantt) car ça rassure les décideurs, les gens ayants l’argents.

--

![](attachements/module_5-wehaveaproblem.gif)
notes:
Mais il se trouve que ce modèle pose pas mal de problèmes !

--

![](attachements/module_5-chain.png)

notes:
Si un problème survient à n’import quelle moment dans la chaine de création, il faut revenir en arrière et repasser à travers toute les étapes ! C’est couteux en temps et en argents…

--

![](attachements/module_5-error.jpg)

notes:
On a vu précédemment qu'avec le modèle en waterfall, le produit final ne convenais pas toujours au client, C’est encore la rigidité de ces méthodes qui sont en jeux qui pose problème. C’est directement liée à la verticalité hiérarchique…

--

![](attachements/module_5-hierachy.svg)

notes:
En effet, comme on l’a vu durant le dernier cours la hiérarchie verticale n’aide pas car elle :

-   rajoute des relais
-   provoque des problèmes de communication entre les départements: des silos

On s’est mis à ignorer notre nature sociale en creusant des silots…

--

![](attachements/module_5-fail.gif)

notes:
Bref, lorsqu’on a un problème, peu import d’où il vienne, ça fait plutot mal… en terme d’argent, et surtout humainement.

Nos méthodes de production actuelle ne sont pas centrée sur la communication, elle sont focalisées sur les problématiques de budget et de rentabilité. Seulement, sans prendre en compte l’aspect social qui est au coeur de notre nature, elle négligent des aspects qui pourraient tout autant lui apporter.

--

![](attachements/module_5-fast.gif)
notes:
Au final vu qu’on était sensé avoir tout plannifié dès le début avec le fameux gantt, on se retrouve à devoir accélérer la cadence en fin de production pour respecter des deadlines fixés à coup de millier de dollards. C’est l’une des causes direct des fameux crunch time qu’on rencontre dans le jeux video… :/

--

<img src='attachements/module_5-wall.jpg' width=800>


notes:
Mais la collaboration temps réel à le pouvoir de changer les chose ! De casser les murs entre les départements

Métaphore de l’avion:

un projet c’est un peu comme un avion, il est difficile de prévoir la météo sur le long terme, vaut mieux apprendre à bien piloter que perdre du temps à trouver des prévisions approximatives.

--

### Dans une galaxie pas si lointaine…

notes:
Il faut savoir que c’est pas quelque chose de nouveau :) Ça s’est même standardisé dans d’autre milieu !

--

![](attachements/module_5-googledoc.png)

notes:
L’exemple le plus courant reste celui de google doc qui s’est imposé dans le milieu du traitement de texte. Aujourd’hui c’est devenu banal d’éditer un document à plusieurs ^^ Dans certain cas on imagine même plus faire sans hehe.

--

![](attachements/module_5-bim.jpg)

notes:
Dans le BTP ils ont standardisés la collaboration entre les différents corps de métier à travers le format BIM Building information modeling (architectes, ingénieurs et entrepreneurs).

Le format permet de modéliser les informations de tout les corps de métier et de voir l’intéraction entre les couches !

Ils ont bien compris qu’il y avait beaucoup à y gagner ^^

-   productivité (donne de nouvelles facons d’acomplir un projet)
-   argent économisé et argent (erreurs anticipés)
-   prise de décision précise

On fait préhistorique avec nos méhtodes à côté :) Mais l’USD pourrait devenir notre BIM à nous ! \o/

--

![](attachements/module_5-agile_methodology_maps.png)

notes:
Le monde du dev logiciel n’est pas en reste non plus ! Vous vous souvenez de cette carte évoquée la semaine dernière ? Et bien il se trouve que l’un des outils de ces méthodes se base sur la collaboration tps réel. Le pair programming !

--

![](attachements/module_5-pair_programming.gif)

---

![](attachements/module_5-where.gif)

notes:
Mais nous alors, où on en est ? Où est-ce que le collaboratif tps réel est ou sera impliqué dans notre domaine ?

---
<!-- .slide: data-stack-name="Définition" -->

## Partager le processus de création

--

![](attachements/module_5_partage_processus_creation.png)

--

![](attachements/module_5-cross-mono-dcc.png)

--

### Cross-DCC

![](attachements/module_5-cross-dcc-usd.png)

--

### Mono-DCC

![](attachements/module_5-cross-dcc-advtg.png)

--

### Mono-DCC

![](attachements/module_5-mono-dcc-blender.png)

--

![](attachements/module_5-previz.jpg)

notes:
**À la croisée des mondes**

Finalement c’est plutôt du côté de la préviz que le collaboratif tps réel à commencé à percer.

C’est logique car c’est l’une des étapes de production qui exploite du cinéma qui exploite le plus le temps réel.

-   Les moteurs de rendu tps réel y sont courants
-   C’est une étape qui met en relation beaucoup de corps de métiers
-   C’est un peu comme une scène au théâtre, imaginez que les éclairagistes travaillent pas dans la même temporalité que les acteurs.

==> la collaboration est donc au coeur du processus

---

### Les solutions existantes

notes:
On va peindre ensemble le paysage temporel de ce qui existe

--

#### Les solutions de collaboration 3D actuelles

![](attachements/module_5-collab-solution.svg)


---

![](attachements/module_5-collab_dcc_2.svg)
--

![](attachements/module_5-collab_dcc_connection_2-1.svg)

---
<!-- .slide: data-stack-name="Organisation" -->

## Comment ?

![](attachements/module_5-how.gif)
notes:
La nature linéaire des méthodes actuelles nécessite de revoir profondément nos manière de produire. Les méthodes agiles sont une bonne piste !

Après l’adoption de la collaboration temps réel ne se fera pas sans une remise en question profonde de nos modes de productions ! Et c’est là que les méthodes agiles peuvent se réveler utiles \o/

L’appropriation du temps réel au sein de la production ne peut se faire sans cet effort. C’est un changement de paradigme qui bouscule nos habitudes…

On va voir que de par bien des aspects, cette méthode rejoint ce que nous avons vu au cours précédent.

--

### Équipes

-   Spécialisées ou interdisciplinaires
-   6 personnes maximum
-   Mélanger les niveaux d'expérience

notes:
Durant mes recherches j’ai exploré les deux possibilités.

--

![](attachements/module_5-team_composition.svg)
notes:
Dans les deux cas, certains poins commun en sont ressortis. Un nouveau rôle est nécessaire: celui de coordinateur. C’est la personne qui à un peu un point de vu globale sur ce qui se passe et qui peut fluidifier l’expérience.

Après c’est vital qu’il y ai un niveau mixte au sein des équipes. Pourquoi ? Pour avoir un oeil expérimenté, pour la transmission du savoir, …

--

### Workflow

notes:
On va voir brièvement comment gérer le workflow dans un environment de collaboration temps réel.

--

#### Segmentation des tâches

![](attachements/module_5-task-segmentation.png)

--

#### Temporalité  d'une session

--

<!-- .slide: data-transition="slide-in none" -->

![](attachements/module_5-session_time_0.svg)

--

<!-- .slide: data-transition="none" -->

![](attachements/module_5-session_time_1.svg)
notes:
Finalement un peu comme dans un orchestre, le warmup c’est l’accordage des violont. C’est une étape fondamentale pour l’efficacité de la session qui suivra.

Sans cela, les artistes risquent d’être déphasés et dirigés dans de mauvaises directions. C’est là qu’on pose les fondations, les règle de la session et surtout la répartition initiale des tâches.

Un peu comme le sprint planning !


--
<!-- .slide: data-transition="none" -->


![](attachements/module_5-session_time_2.svg)
notes:
Ce qui est extrêmement intéressant ça va etre l’apparition naturelle d’un phénomène d’auto évaluation qui va pousser les artistes a recalibrer leur travail en fonction ce qui est en cours de construction.

--
<!-- .slide: data-transition="none" -->

![](attachements/module_5-session_time_4.svg)
notes:
Il ne faut pas avoir peur de revenir sur une tâche ! Ça fait partie des bénéfices de la méthode. On peut corriger le tir

--
<!-- .slide: data-transition="none" -->

![](attachements/module_5-session_time_5.svg)
notes:
Il est aussi courant de continuer une tâche entamée par un autre artiste.

--

### Dataflow

--


![](attachements/module_5-file_share_classic.svg)

*Dataflow classique*

--

![](attachements/module_5-file_share_collab.svg)

*Dataflow collaboration temps réel*

notes:
Noter qu’actuellement **toutes** les solutions se basent sur un modèle **centralisé** !

--

![](attachements/module_5-teamwork.png)
notes:
En collaboratif temps-réel, le problème du dataflow se pose à l’intérieur de la scène même. Comment structurer le scène graph de manière à naviguer facilement ?

--

<img src='attachements/module_5-hierarchy_artist_centric_1.svg' width=1000>
notes:
La hiérarchisation du scene graph en task centric favorise l’émergence d’une conscience de groupe vis à vis du projet !

--

Collaboration temps réelle + Scrum = ♥

---

### Des questions en suspens

--

#### versioning ?

--

#### Rigs ?

---
<!-- .slide: data-name="Tour du multiuser" -->

## Introduction au Multiuser

![](attachements/module_5-example.gif)

---

## Des questions ?
# Pipeline pour les productions temps réel

---

<!-- .slide: data-stack-name="Différences avec le précalculé" -->
## Différences structurelles

--

### Complexité du pipeline


| ![](attachements/module_3-film_assets.gif) <!-- .element: class="fragment" data-fragment-index="2" --> | ![](attachements/module_3-game_assets.gif) <!-- .element: class="fragment" data-fragment-index="4" --> |
| :------------------------------------------: | :------------------------------------------: |
| *Dans le pré-calculé* <!-- .element: class="fragment" data-fragment-index="1" -->                      | *Dans le temps réel* <!-- .element: class="fragment" data-fragment-index="3" -->                       |


notes:
Comme on l’avait évoqué la dernière fois dans les asset manager.
Là où dans un film on a relativement peu de types d’asset,
Dans un jeu video on doit jongler **SIMULTANÉMENT** avec pleins types de ressources différentes.

--

### Catégories d’asset

- Work files<!-- .element: class="fragment" -->
- Games assets<!-- .element: class="fragment" -->

notes:
Les DCC utilisé pour créer les asset artistiques comme blender ou Maya ne fournissent pas toutes ou trop d’informations par rapport aux besoins du moteur. Il est donc nécessaire de passer par un format binaire intermédiaire qui fera tampon entre le moteur et le DCC et permettra de faire **l’intégration**. Souvent on se retrouvera donc avec deux grandes catégories d’asset :

-   Les work files
-   Les games assets

On reviendra plus tard sur la manière dont transitent les données d’un logiciel à un autre. Pour l’instant restons en à ces deux catégories…

--

### Segmentation 

- Levels<!-- .element: class="fragment" -->
- Section<!-- .element: class="fragment" -->

notes:
Nous avions aussi vu que lors de la description de l’étape de production que l’assemblage des assets différait aussi du pré-calculé : Dans un Film on parle en **sequence** & **shots** alors que dans le jeu vidéo on découpe l’espace en **level** et **section**.

Les **sections** sont enfait des portion de level streamées individuellement.

--

## Nomenclature

--
#### Un exemple de structure de dossiers en miroir

notes:
En pennant en compte les différences expliquées juste avant on obtiens quelque chose comme ça

--
<!-- .slide: data-transition="slide-in none" -->
![](attachements/module_3-nomenclature_1_1.svg)


notes:
 On peut qualifier ça de structure en miroir car la structure de fichier est dupliquée en suivant la logique d’asset de travail et de game asset… On a donc deux dossiers principaux…

--
<!-- .slide: data-transition="none" -->

![](attachements/module_3-nomenclature_2.svg)

notes:
Les assets sont ensuite répartis principalement selon leur spatialisation (level/section/general).

--
<!-- .slide: data-transition="none slide-out" -->

![](attachements/module_4-nomenclature_3.svg)


notes:
Les levels sont souvent séparés en sections…

Dans certaines situation on peut éviter d’avoir à passer par cet import/export. Lorsque l’on est amené à utiliser des outils qui vont exporter directement le contenu pour les moteurs de jeu, les dossiers correspondant n’existeront que dans les _gameAsset_. Après cela dépend aussi des outils présent dans le moteur. Certains détecterons des changements automatiques…

La nomenclature va beaucoup dépendre du moteur utilisé car les types d’assets présents dépendent de ce dernier. Cheque moteur donnes ses guidelines.

---

### Ingestion des assets

![](attachements/module_3-asset-ingestion.gif)
notes:
Pourquoi devoir faire un export et un import ?

Il existe des moyens de passer outre mesure… ce cependant **on perd en flexibilité**.

Garder les exports/import permet de reformater les fichiers tampons sans demander aux artistes de ré-exporter… Permet aussi de reformater les assets pour différentes plateformes sans aucuns effort de l’équipe artistique.

Pas mal d’outils automatisent le processus(Donner l’exemple de l’exporter Blender d’Unreal)

Export: 
- enlève les données inutiles au moteur de jeu
  
Import: 
- Converti les données exportés en un format optimisé orienté pour les besoins du _game engine_ 
- Responsable d’un gros **lifting** des données: 
	- Génère les collisions 
	- Génère des données de lighting dans certains cas

Mais finalement quel format de fichier est le mieux adapté pour gérer cet export ?

---
<!-- .slide: data-stack-name="Standards" -->
## Standards pour le temps réel

notes:
Nous allons faire un petit topo des formats utilisés pour exporter les données d’un DCC vers un game engine.

Pour éviter la redondance je passerai assez vite sur ceux qu’on a étudié la semaine dernière. On va voir 3 principaux format

-   OBJ
-   FBX
-   GLtf

--

<img src="attachements/module_3-obj.png" alt="" width="200">

![](attachements/module_3-feature_obj.png)

notes:
Supporte que de la géométries, même pas les transform…

--
![](attachements/module_3-old.gif)

--
![](attachements/module_3-fbx.png)

![](attachements/module_3-fbx_features.png)
notes:
FBX = filmbox format propriétaire détenu par Autodesk…

C’est un format qui c’est imposé comme standart pour les échanges de données 3d. Cependant Autodesk n’a jamais publié de standard…

Cela pose problème pour les projets opensouce par example, les utilisateurs doivent accepter une EULA(end-user license agreement)…

Godot et Blender ont par example écrit leur propre import/export…

Un des rares avantage du FBX réside dans le fait que ce soit un format binaires, ce qui rend sont parsing rapide…

--

<img src='attachements/module_2-gltf.png' width=300/>

![module_2-feature-gltf](attachements/module_2-feature-gltf.png)

notes:
Développé par Khronos avec la collaboration de tout les gros de l’industrie (Microsoft, Unity, Google, Adobe, NVidia, Oculus, etc.).

Actuellement en version 2

Point de vue du developer de Godot:

-   JSON based
-   Separate binary blob for arrays. permet de gerer les gros volumes de donnée dans des fichiers binaires. il prend même en charge de nombreux types de données GPU courants, de sorte qu’en pratique, ce fichier peut être déplacé par morceaux directement dans la mémoire GPU
-   Modern features
    -   skeletons and morph targets
    -   PBR based materials using the Disney/Unreal format
    -   [Clean documentation](https://github.com/KhronosGroup/glTF/tree/master/specification/2.0)
-   Best of all, it’s completely open and everyone can contribute!

--

-   OpenGEX
-   Open Collada

notes:
J’en ai volontairement omis deux et je vais vous expliquer pourquoi :

-   OpenGEX:
    -   Pas vraiment un standard
    -   Développé par une seule personne
-   OpenCollada, bien que supporté preseuqne partout, il est tombé en désuétude :
    -   XML based format, les données textuelles doivent etre parsés…
    -   Specifications ambigues sur plein d’aspects comme la manière dont doivent être exporté les données, squelettes, et autre. Le format laisse beaucoup de liberté sur les unités et spécification de transform(Quel est en haut par exemple) utilisés aux exporter. Du coup c’est beaucoup de travail de parser le format…
    -   La majorité des exporter existant sont pas fonctionnels (information manquantes, spec non respéctée, pas d’id uniques…)
    -   Le temps de chargement est lent (text format)
    -   Autodesk à travaillé contre le format en adoptant un exporter incomplet
    -   Pareil pour Blender

--

<img src='attachements/module_3-game_dcc_standards.svg' width=1200/>

Liste des compatibilités entre standard & DCC.

---

### Asset managers

notes:
Comme on l’avait vu tout à l’heure, la gestion des dépendances est beaucoup plus complexe pour un projet temps-réel…

Lors de la description de structure tout à l’heure j’ai volontairement omis la partie nomenclature car cette dernière va directement dépendre du moteur utilisé.

En effet, chaque moteur possède ses **types** d’assets. C’est aussi pourquoi généralement, les game engine générique on un asset manager intégré. Pour gerer efficacement les données qui leur sont propre.

Ces données étant en général binaire et optimisés, il est nécessaire d’avoir une interface graphique dédiée pour les manipuler et les visualiser: un asset manager.

--

![](attachements/module_3-unity_asset_manager.png)

Asset manager d’unity.

--

![](attachements/module_3-godot_asset_manager.png)

Asset manager de Godot.

--

![](attachements/module_3-ue4_asset_manager.png)

Asset manager d’unreal.

--

Un asset manager permet de:
- Visualiser<!-- .element: class="fragment" -->
- Manipuler<!-- .element: class="fragment" -->

notes:
En général ces derniers vont permettre de :

- visualiser: 
	-   Dépendances
	-   Audit
- manipuler:
	-   Modifications
	-   IO
	-   l’import/export des assets

--


<img  src='attachements/module_3-ue4_asset_dependencies.png' width=1000>

Visualisation des dépendances dans Unreal.

--

<img  src='attachements/module_3-ue4_asset_dependencies_complex.png' height=800>

Visualisation des dépendances dans Unreal.

--

![](attachements/module_3-asset_audit.png)

Audit des assets dans Unreal.

--

<img  src='attachements/module_3-central_project.svg' width=800>

notes:
Si on résume ce qu’on a dit jusque maintenant :
-   On a différents départements qui vont créer des assets
-   On a vu que les games asset contenaient aussi bien du code que des assets graphiques
-   On a vu que les assets artistiques notamment passaient par un export/import dans le moteur
-   On a vu pouvaient structurer les hiérarchies de dossier pour stocker assets

Les games asset sont donc créées par différents départements.

--

![](attachements/module_3-sync.gif)

notes:
Au final on a pas moins de quatre départements qui travaillent dans le même projet dans le game engine !

A partir de ce moment là il va être crucial de synchroniser ce travail centré autour du game engine et de savoir QUI à fait QUOI et QUAND ?

L’enjeux ça va être d’eviter les conflit, les frictions dans le projet. Et pour ça rien de mieux que le **versioning** !

---

<!-- .slide: data-name="Versionning" -->

## Versioning

![](attachements/module_3-tracking.gif)

notes:
On a vu : 
- comment generer 
- structurer un projet de jeu, cependant il manque une brique importante vital à la collaboration d’un projet temps-réel… LE VERSIONNING

---

### Version control system (VCS)

kesako ?

![](attachements/module_3-kesako.gif)

notes:
Lors du premier cours on avait vu que le temps réel et le précalculé avaient deux stratégies différentes. En précalc on a une politique de versionning basique ( on avait vu vite fait (LOL) quon pouvais se baser versions basées sur l’incrément d’indices) En temps réel, on avait parlé de polique stricte. Mais qu’est-ce que ça veux dire ?

Concrêtement ça veux dire qu’on va utiliser une solution dédiée au contrôle de version. La fonction de cette dernière va être de d’enregistrer les changements qui se produise sur un ou plusieurs fichier en fonction du temps de manière à pouvoir réetablir une version specifique plus tard.

Ça peut s’apliquer à quesiment n’importe quel type de fichier. Globalement ça vous permet de revert un projet à une ancienne version, de voir qui est le dernier à avoir fait quelle modification qui peut avoir causé problème… C’est aussi une toile de sécurité, si vous cassez tout localement, vous pouvez facilement récuperer.

-   intérêts:
	-   Pour l’individu : toile de sécurité
	-   pour l’equipe: garder les ressource du projet à jour
	-   Ça permet de faire travailler l’équipe en continue mais ça nécessite de la discipline ! Le problème que ça amène c’est que chacun travail sur son petit univers déconnecté du flux de la production. Des decisions artistique sont basé sur des asset ayant déjà changé, et des descision trechniques peuvent etre basé sur des features qui ne sont plus là ! [@dunlopProductionPipelineFundamentals2014, p137]


--

### Avant de commencer

[![](attachements/module_5-git_book.png)](https://www.git-scm.com/book/en/v2)

--

### Types de version control

notes:
Il existe trois grandes catégorie de solution de version contrôle:

-   Local
-   Centralisés
-   Décentralisés

Je vais passer sur les trois pour vous donner un point de vue global de la chose

--

![](attachements/module_3-local.png)

*VCS Local*

notes:
Finalement c'est un peu ce que les gens font lorsqu'ils font un versionning manuel... Cependant des erreur arrivent vite !

Pour résoudre ces soucis des dev on mis en place une solution (RCS) qui stocke l’historique sous forme de différentiels dans une base de donnée locale. Ça peut dont ensuite reconstituer le fichier à n’importe quel moment dans le temps en additionnant les diff/patchs.

Le problème c’est que c’est mission impossible de collaborer à plusieurs sur le même projet avec ça du coup on a fait…

--
#### Exemple:
- [GNU RCS](https://www.gnu.org/software/rcs/)

--

![](attachements/module_3-centralized.png)

VCS Centralisé


notes:
Le système de version control centralisé à été mis en place pour répondre à ça. (CVCS).

Un seul serveur va stocker toutes les versions des fichiers. Les clients vont récuperer localement que la version souhaité. C’est un standart asser répandu.

Avantages:

-   Chacuns connait jusqu’a un certain degrée ce sur quoi les autres travaillent
-   les administrateur peuvent controler avec une granularité très fine sur qui peut faire quoi

Cependant ça à aussi de gros inconvénients !

-   Single point of failure: Si le serveur ou la connection tombent, pllus personne ne peut collaborer du tout et plus sauver de version Si jamais le disque dur stockant les snapshot lache, vous perdez tout l’historique du projet. Le fait de stocker l’historique du projet à un seul endroit est donc un risque à envisager lorsque qu’on utilise un CVCS.

--

#### Exemples:
- Subversion
- Perforce

--

![](attachements/module_3-distributed.png)

*VCS Distribué*

notes:
C’est pour ces raisons que les systèmes de distrubted version control on vu le jour. Chaque client fait un mirroir complet du repository. Si un serveur tombe, n’importe lequel des client peut recopier son mirroir pour le restorer. Qui plus est les DVCS gèrent en général plusieurs remotes simultanément, vous pouvez donc collaborer avez plusieur groupe de personnes de différentes manière au sein du même projet !

--


#### Exemples:
- Mercurial
- Git

notes:
Ce qui nous amène donc à GIT \o/ La solution que je vais vous introduire et qu’on utilisera durant le TD. Pour la petite histoire, git

---

#### Introduction à GIT

notes:
Pour la petite histoire, Git à été développé par la communauté de linux pour gérer le développement du Kernel Linux en 2005.

--

#### Qu’est ce que git ?

notes:
Pour comprendre git, il faut commencer par la base. Il faut savoir que Git stocke ses données d’une manière un peu différente de la majorité des VCS.

--

![](attachements/module_3-deltas.png)

Delta-based version control utilisé dans la majorité des VCS.

notes:
Généralement les VCS(Subversion, Perforce, etc.) considèrent les informations qu’ils stockent comme un ensemble de fichiers et les modifications apportées à chaque fichier au fil du temps. Ça prend donc la forme de Delta.

--

![](attachements/module_3-snapshots.png)

Flux de snapshots utilisé par **Git**.

notes:
Git gère ses données comme un flux de snapshot. À chaque révision git stocke une “image” de tout le projet à cette instant

--

#### Les trois états

-   modified
-   staged
-   committed

notes:
-   modified signifie que vous savez modifié le fichier mais il n’est pas ajouté à la base de donnée.
-   staged signifie que vous avez marqué le fichier modifié dans sa version actuelle pour etre mis dans le prochain snapshot (commit)
-   committed signifie que la modification est stocké de manière sur dans la base de donné (repository)

--

![](attachements/module_3-areas.png)

Les trois “zones” de **Git**.

notes:
**Working directory** c’est le répertoire de travail courant. Celui dans lequel vous faites votre popote. Finalement c’est une version du projet à un instant T.

**Staging area** c’est un fichier stocké dans `.git` qui contient les infos des fichiers qui iront dans le prochain commit.

**Git directory** c’est là où git va stocker la base de donnée et les metadata du repository. C’est la partie la plus importante de Git, c’est ce qui est copié lorsque vous `clonez` un repository.

The basic Git workflow goes something like this:

-   You modify files in your working tree.
    
-   You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
    
-   You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Git directory.

--

#### Accéder à Git

notes:
En général on peut utiliser git soit via...

--

#### Ligne de commande

![|200x](attachements/module_3-git_terminal.png)

*Git bash*

--

#### Interface graphique

<img src='attachements/module_3-git_extension.png' width=1000>

Git Extension

notes:
Aborder les avantages et inconvénients… Maintenant on va rentrer dans le vif du sujet. On va s’intéresser a git en ligne de commande

---

## Des questions ?

